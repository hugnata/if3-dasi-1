/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.dao;

import fr.insalyon.dasi.metier.modele.Intervention;
import fr.insalyon.dasi.metier.modele.Personne;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author hreymond
 */
public class InterventionDao {
    
    
     /**Met à jour une intervention en base de données
     *
     * @param i L'intervention à mettre à jour
     * @return
     */
    public static Intervention Update(Intervention i){
        EntityManager em = JpaUtil.obtenirEntityManager();
        return em.merge(i);
    }

    /**Crée une intervention dans la base de donnée
     *
     * @param i
     */
    public static void Create(Intervention i){
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.persist(i);
    }
    
    /**Charge une intervention par son identifiant
     *
     * @param id
     * @return
     */
    public static Intervention Load(int id){
        EntityManager em = JpaUtil.obtenirEntityManager();
        return em.find(Intervention.class,id);
    }

    /**Supprime une intervention en base de donnée
     *
     * @param i l'intervention à supprimer
     */
    public static void Delete(Intervention i){
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.remove(i);
    }

    public static List<Intervention> getInterventionsDuJour() {
       EntityManager em = JpaUtil.obtenirEntityManager();
       Date hier = new Date();
       hier.setDate(hier.getDate()-1);
        System.out.println(hier);
       String jpql = "select i from Intervention i where i.debut > :hier";
       Query query = em.createQuery(jpql);
       query.setParameter("hier",hier,TemporalType.TIMESTAMP);
      
       
       return (List<Intervention>) query.getResultList();
    }
}
