package fr.insalyon.dasi.metier.service;

import com.google.maps.model.LatLng;
import fr.insalyon.dasi.dao.EmployeDao;
import fr.insalyon.dasi.dao.InterventionDao;
import fr.insalyon.dasi.dao.JpaUtil;
import fr.insalyon.dasi.dao.PersonneDao;
import fr.insalyon.dasi.metier.modele.Client;
import fr.insalyon.dasi.metier.modele.Employe;
import fr.insalyon.dasi.metier.modele.Intervention;
import fr.insalyon.dasi.metier.modele.Personne;
import fr.insalyon.dasi.util.DebugLogger;
import fr.insalyon.dasi.util.GeoTest;
import fr.insalyon.dasi.util.Message;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import org.apache.derby.shared.common.error.DerbySQLIntegrityConstraintViolationException;

/**
 *
 * @author hreymond
 */
public class Service {
    //Permet d'afficher la date dans un format lisible
    private final static SimpleDateFormat HORODATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy à HH:mm:ss");
    
    
     /***Permet d'authentifier un client ou un employé
     * 
     * @param nomutilisateur Le nom d'utilisateur du client/employé
     * @param motdepasse Le mot de passe de l'utilisateur
     * @return la personne qui a tenté de se connecter si l'authentification a réussi, Null sinon
     * 
     * @throws IllegalAccessException Si le mot de passe est le mauvais
     * @throws NoResultException si l'utilisateur est inconnu
     * @throws ConnectException si la connexion à la base de donnée a échoué
     */
    public static Personne authentifier(String nomutilisateur,String motdepasse) throws IllegalAccessException,NoResultException,ConnectException{
              
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
        Personne p = PersonneDao.LoadByUsername(nomutilisateur);
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();
         if(p.getPassWord().equals(motdepasse)){
            DebugLogger.log("L'utilisateur " + nomutilisateur + " s'est connecté");
            return p;
        }else{
            IllegalAccessException exception = new IllegalAccessException("Mot de passe incorrect !");
            DebugLogger.log("Le mot de passe rentré est incorrect ",exception);
            throw exception;
        }
       
    }
    
    /**Inscrit un client à l'application
    * 
    * <p>
    * La méthode calcule les coordonées GPS, enregistre les données du client dans la base de donnée et lui envoie un mail
    * </p>
    * 
    * @param c Le client a enregistrer
    * 
    * @return Le status de la création (Réussi/Echoué)
    * 
    * @throws ConnectException si la connexion à la base de donnée a échoué
    * @throws DerbySQLIntegrityConstraintViolationException si l'username existe déja
    */
   public static boolean inscrireClient(Client c) throws ConnectException,DerbySQLIntegrityConstraintViolationException{
        
        boolean reussi =false;

        DebugLogger.log("Enregistrement d'un nouveau Client: " + c.getNom() + " " + c.getPrenom());
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
        
        DebugLogger.log("Calcul de la position GPS");
        LatLng positionGPS =  GeoTest.getLatLng(c.getAdresse());      
        if(positionGPS ==null){
            Message.envoyerMail("ne-pas-repondre@proactif.com", c.getMail(), "Compte client échoué", "Votre compte client n'a pas été créé, adresse non trouvée");
        }else if(PersonneDao.usernameExist(c.getUserName())){
            Message.envoyerMail("ne-pas-repondre@proactif.com", c.getMail(), "Compte client échoué", "Votre compte client n'a pas été créé, nom d'utilisateur déja pris");
        }else{
          c.setLatitude(positionGPS.lat);
          c.setLongitude(positionGPS.lng);
          PersonneDao.Create(c);
          Message.envoyerMail("ne-pas-repondre@proactif.com", c.getMail(), "Compte client créé", "Votre compte client a été créé avec succès, votre code client est le suivant: " + c.getId());
          reussi = true;
        }
  
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();
                  
   
        return reussi;
   }
   
     /**Inscrit un client à l'application
    * 
    * <p>
    * La méthode calcule les coordonées GPS, enregistre les données du client dans la base de donnée et lui envoie un mail
    * </p>
    * 
    * @param c Le client a enregistrer
    * 
    * @return Le status de la création (Réussi/Echoué)
    * 
    * @throws ConnectException si la connexion à la base de donnée a échoué
    * @throws DerbySQLIntegrityConstraintViolationException si l'username existe déja
    * @throws NullPointerException nottament si l'adresse n'est pas trouvée
    */
   public static boolean inscrireEmploye(Employe e) throws ConnectException,DerbySQLIntegrityConstraintViolationException,NullPointerException{
        
        DebugLogger.log("Calcul de la position GPS");
        LatLng positionGPS =  GeoTest.getLatLng(e.getAdresse());   
        if(positionGPS==null){
           throw new NullPointerException("Adresse non trouvé...");
        }
        e.setLatitude(positionGPS.lat);
        e.setLongitude(positionGPS.lng);

        DebugLogger.log("Enregistrement d'un nouvel Employe: " + e.getNom() + " " + e.getPrenom());
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
        PersonneDao.Create(e);
        JpaUtil.validerTransaction();
        JpaUtil.fermerEntityManager();

        Message.envoyerMail("ne-pas-repondre@proactif.com", e.getMail(), "Bienvenue dans l'entreprise", "Bienvenue dans l'entreprise " +  e.getPrenom() + " "+e.getNom());
        return true;
   }
   
   /**Réalise une demande intervention 
    * 
    * La fonction permet de demander pour une intervention donnée et un client donnée l'attribution d'un employé disponible.
elle envoie ensuite un notification à l'employé le plus proche pour qu'il prenne en compte sa demande.
    * 
    * 
    * @param intervention L'intervention en question
    * @param client Le client qui fait la demande
    * 
    * @return Le résultat de l'opération: True si un employé est affecté, False si la demande est refusée
    */
   public static Intervention demandeIntervention(Intervention intervention,Client client){
        
       //Recherche de l'employé disponible le plus proche
        DebugLogger.log("Recherche d'un employé disponible...");
        boolean reussi = false;
        

        // Acquisition de tous les employes dispo
        JpaUtil.creerEntityManager();
        JpaUtil.ouvrirTransaction();
        List<Employe> lesEmployes = EmployeDao.getEmployeDispo();
        JpaUtil.validerTransaction(); 
        
        DebugLogger.log(lesEmployes.size() + " employés disponibles");
        
        //Calcul de la position du client
        LatLng positionClient = new LatLng(client.getLatitude(),client.getLongitude());
        DebugLogger.log("Recherche autour de la position: "+ positionClient.toString());
        
        //Si des employés sont disponibles
        if (lesEmployes.size()!=0){
            
            //On cherche les employés les plus proches
            double min = GeoTest.getTripDurationByBicycleInMinute(new LatLng(lesEmployes.get(0).getLatitude(), lesEmployes.get(0).getLongitude()),positionClient );
            Employe plusProche = lesEmployes.get(0);
            
            for(Employe e:lesEmployes){
               DebugLogger.log("\tEmploye: "+ e.getNomEntier() + ", "+  new LatLng(e.getLatitude(), e.getLongitude()));
               double distance = GeoTest.getTripDurationByBicycleInMinute(new LatLng(e.getLatitude(), e.getLongitude()),positionClient );
               
               if (distance<=min) {
                   min = distance;
                   plusProche = e;
               }
            }
            
            
          
            
          
            JpaUtil.ouvrirTransaction();
            //On attribue l'intervention à l'employé le plus proche
            plusProche.attribuerIntervention(intervention);
            //On ajoute l'intervention à celle des clients
            client.addIntervention(intervention);
            InterventionDao.Create(intervention);
    
            PersonneDao.Update(client);
            PersonneDao.Update(plusProche);
            JpaUtil.validerTransaction(); 
            
            
            Message.envoyerNotification(plusProche.getTelephone(),
                    "POUR: " + plusProche.getNomEntier()+"\n"+
                    "TEL: "+ plusProche.getTelephone() + "\nMessage: " + 
                    "Intervention " + intervention.getClass().getSimpleName() + " demandée le "+ HORODATE_FORMAT.format( intervention.getDebut()) + 
                   " pour " + intervention.getClient().getNomEntier() + " (#"+ intervention.getId() + ")," + intervention.getClient().getAdresse() +
                    " \" "+ intervention.getDescription() + " \"."  );
       
        }else{
            DebugLogger.log("La demande n'a pas pu être prise en compte");
            intervention =null;
        }
        
        
        
        
        JpaUtil.fermerEntityManager();
        
        
          
        return intervention;
   }
   
   /**Permet de récupérer les interventions liées à un client
    * 
    * @param c Le client concerné
    * @return La liste des interventions liées au client
    */
   public static List<Intervention> getInterventions(Client c){
       if(c.getListeIntervention()==null){
           return null;
       }else{
           c.getListeIntervention().size(); // cette ligne ne sert concretement à rien, mais elle sert juste à mettre a jour la liste qui est géré par la base de données (c'est le prof qui nous l'a dit)
           return c.getListeIntervention();
       }
   }
   
   /**Permet de récupérer la liste des interventions du jour
    * 
    * @return 
    */
   public static List<Intervention> recupererInterventionsDuJour(){
        JpaUtil.creerEntityManager();
        List<Intervention> interventions = InterventionDao.getInterventionsDuJour();
        JpaUtil.fermerEntityManager();
        
        return interventions;
   }
    
   
   /**Permet de valider une intervention
    * 
    *<p>Attention ! Il faut valider une intervention uniquement lorsque le commentaire a été rempli</p>
    * 
    * @param intervention L'intervention à valider
    * @return L’intervention validée en  cas de réussite, null sinon
    * @throws NullPointerException si des informations sont manquantes
    */
   public static Intervention validerIntervention(Intervention intervention) throws NullPointerException{
       
       if(intervention==null || intervention.getCommentaire()==null || intervention.getClient()==null || intervention.getDebut()==null){
           throw new NullPointerException("Des informations sont manquantes");
       }
       if(intervention.getStatus()==0){
           throw new NullPointerException("Veuillez mettre à jour le  status de l'intervention");
       }
       intervention.setFin(new Date());

       JpaUtil.creerEntityManager();
       JpaUtil.ouvrirTransaction();
       Message.envoyerNotification(
               intervention.getClient().getTelephone(),
               "Pour : " + intervention.getClient().getNomEntier()+"\n"+
               "Tel : "+intervention.getClient().getTelephone()+"\n"+
               "Message : Votre demande d'intervention du " + HORODATE_FORMAT.format(intervention.getDebut()) + " a été cloturée le "
               + HORODATE_FORMAT.format(intervention.getFin()) + ". " + intervention.getCommentaire()
       );
       intervention.getEmploye().validerIntervention();
       InterventionDao.Update(intervention);
  
       PersonneDao.Update(intervention.getEmploye());
       JpaUtil.validerTransaction();
       JpaUtil.fermerEntityManager();
       
       
       return intervention;
   }
}
