/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.metier.modele;

import fr.insalyon.dasi.util.Message;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

/**
 *
 * @author mmessalti
 */
@Entity
public class Client extends Personne{
    // le numero de client, c'est l'id de personne
    
    @OneToMany(mappedBy = "client",cascade = CascadeType.ALL)
    private List<Intervention> listeIntervention;

    /**
     *
     */

    public Client() {
        super();
    }
    
    

    /**
     *
     * @param userName
     * @param passWord
     * @param telephone
     * @param civilite
     * @param nom
     * @param prenom
     * @param dateNaissance
     * @param adresse
     * @param mail
     */
    public Client(String userName, String passWord, String telephone, char civilite, String nom, String prenom, Date dateNaissance, String adresse, String mail) {
        super(userName, passWord, telephone, civilite, nom, prenom, dateNaissance, adresse, mail);
       
    }

    public List<Intervention> getListeIntervention() {
        if(listeIntervention==null){
            return null;
        }
        listeIntervention.size();
        return listeIntervention;
    }

    public void setListeIntervention(List<Intervention> listeIntervention) {
        this.listeIntervention = listeIntervention;
    }
    
    public void addIntervention(Intervention i){
        
        i.setClient(this);
        this.listeIntervention.add(i);
    }

    

}

