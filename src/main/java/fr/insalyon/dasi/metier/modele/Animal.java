/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.metier.modele;

import javax.persistence.Entity;

/**
 *
 * @author mmessalti
 */
@Entity
public class Animal extends Intervention {
    
    /**
     *
     * @param typeAnimal
     */

    private String typeAnimal;

    public Animal() {
        super();
    }
    
    /***Constructeur d'une intervention de type Animal
     * 
     * @param typeAnimal Le type de l'animal
     * @param Description La description du problème
     */
    public Animal(String typeAnimal, String Description) {
        super(Description);
        this.typeAnimal = typeAnimal;
    }
    
    public String getTypeAnimal() {
        return typeAnimal;
    }

    public void setTypeAnimal(String typeAnimal) {
        this.typeAnimal = typeAnimal;
    }

    @Override
    public String toString() {
        return "Animal{ "+super.toString() + ", typeAnimal=" + typeAnimal + '}';
    }
    
    
    
    
}
