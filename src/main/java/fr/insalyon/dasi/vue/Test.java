/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.vue;

import fr.insalyon.dasi.dao.JpaUtil;
import fr.insalyon.dasi.dao.JpaUtil;
import fr.insalyon.dasi.metier.modele.Animal;
import fr.insalyon.dasi.metier.modele.Client;
import fr.insalyon.dasi.metier.modele.Employe;
import fr.insalyon.dasi.metier.modele.Incident;
import fr.insalyon.dasi.metier.modele.Personne;
import fr.insalyon.dasi.metier.modele.Intervention;
import fr.insalyon.dasi.metier.modele.Livraison;
import fr.insalyon.dasi.metier.service.Service;
import java.net.ConnectException;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import org.apache.derby.shared.common.error.DerbySQLIntegrityConstraintViolationException;

/**
 *
 * @author mmessalti
 */
public class Test{
    static Client client;
    static Client client2;
    static Employe emp0;
    static Employe emp1;
    static Employe emp2;
    static Employe emp3;
    static Scanner sc;
    /**
     *
     * @param args
     */
    public static void inscrire(){
        System.out.println("Client (c) ou Employé (e)");
        String qui = sc.next();
        try {
            if(qui=="c"){

                    System.out.println("Numéro ?");
                    int num = sc.nextInt();
                    switch(num){
                        case 1:
                            
                            break;
                        case 2:
   
                            break;
                        default:
                            System.out.println("Connait pas");
                            break;
                    }

            }else{
                            
            }
        } catch (ConnectException ex) {
         } catch (DerbySQLIntegrityConstraintViolationException ex) {
            System.out.println("Username déja pris :");
         }
    }
    public static void main(String[] args) throws ConnectException{
        

            
            System.out.println("Coucou !");
            JpaUtil.init();
            
            System.out.println("###############Génération du jeu de donnée#########");
            /****
             * JEU DE DONNEES
             */
            client = new Client("jean",
                    "motdepasse",
                    "06451378",
                    'H',
                    "DUPONT",
                    "Jean",
                    new Date("10/02/1998"),
                    "Roger Salengro 69100 Villeurbanne",
                    "Jean.DUPONT@gmail.com");
            
            client2 = new Client("pierre",
                    "motdepasse",
                    "06451378",
                    'H',
                    "Pernaud",
                    "Pierre",
                    new Date("10/05/1998"),
                    "Roger Salengro 69100 Villeurbanne",
                    "Pierre.Pernaud@gmail.com");
            
            emp0 = new Employe(8
                    ,18
                    ,"mec"
                    ,"motdepasse"
                    ,"06451378",
                    'H',
                    "CABRONE"
                    ,"Paul"
                    ,new Date("10/02/1998"),
                    "83000 Toulon",
                    "Paul.CABRONE@gmail.com");
            
            emp1 = new Employe("momo","motdepasse"); //Constructeur d'employé avec nom d'utilisateur et mot de passe
            emp1.setAdresse("75000 Paris");
            emp1.setNom("M");
            emp1.setPrenom("Momo");
            emp1.setMail("momo@gmail.com");
            emp1.setDebut(8);
            emp1.setFin(20);
            
            emp2 = new Employe("mec2","motdepasse"); //Constructeur d'employé avec nom d'utilisateur et mot de passe
            emp2.setAdresse("198 avenue Pressense, 69200 Vénissieux");
            emp2.setNom("Dupont");
            emp2.setPrenom("Paul");
            emp2.setTelephone("0698469696");
            emp2.setMail("mec2@gmail.com");
            emp2.setDebut(6);
            emp2.setFin(12);
            
            emp3 = new Employe("mec3","ahD3aing7"); //Constructeur d'employé avec nom d'utilisateur et mot de passe
            emp3.setAdresse("Rue anatole france  69100 Villeurbanne ");
            emp3.setNom("Girard");
            emp3.setPrenom("Yvon");
            emp3.setMail("YvonGirard@teleworm.us ");
            emp3.setTelephone("0698458696");
            emp3.setDebut(9);
            emp3.setFin(13);
            
            System.out.println("Fait !");
            System.out.println("###########Test des services incriptions########");
            
            try {
                Service.inscrireClient(client);
                Service.inscrireClient(client2);
                Service.inscrireEmploye(emp0);
                Service.inscrireEmploye(emp1);
                Service.inscrireEmploye(emp2);
                Service.inscrireEmploye(emp3);

            } catch (DerbySQLIntegrityConstraintViolationException ex) {
                 System.err.println("Username déja pris");
            }
       
        
        
            
            System.out.println("###########Test de l'authentification########## ");
            Personne p2; //L'authentification renvoie un objet de type Personne
            try {
                p2 = Service.authentifier("jean", "motdepasse");
                System.out.println("P2 " + p2 + "\nClasse de P2" + p2.getClass());
                p2 = Service.authentifier("jean", "erroné");
            } catch (IllegalAccessException ex) {
                System.out.println("MDP erroné");
            } catch (NoResultException ex) {
                System.out.println("Login inconnu");
            }
            
            try {
                p2 = Service.authentifier("inconnu", "?");
            } catch (IllegalAccessException ex) {
                System.out.println("MDP erroné");
            } catch (NoResultException ex) {
                System.out.println("Login inconnu");
            }
            
            
            System.out.println("###########Test pour intervention##############");
            System.out.println("1 - Test Création");
            Animal inter = new Animal("Chien","Ma chienne n'a pas de croquettes");
            inter = (Animal) Service.demandeIntervention(inter,client);
            System.out.println("Retour de la demande d'intervention: " + inter);
            
            try {
                client = (Client) Service.authentifier("jean", "motdepasse");
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoResultException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            Incident inter1 = new Incident("Ya le feu chez moi !");
            System.out.println("Retour de la demande d'intervention: "+  Service.demandeIntervention(inter1,client));
            
            try {
                client = (Client) Service.authentifier("jean", "motdepasse");
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoResultException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Liste d'intervention: "+ client.getListeIntervention());
            
            inter.setCommentaire("Votre chienne a été nourrie");
            inter.setStatus(2);
            Service.validerIntervention(inter);
            
            System.out.println("Yvon occupé ?: " + emp3.estOccupe());
            Livraison inter2 = new Livraison("colis","La poste");
            inter2.setDescription("COUCOU");
            
            //Passage coté client
            try {
                client = (Client) Service.authentifier("jean", "motdepasse");
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoResultException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.out.println("Retour de la demande d'intervention: "+  Service.demandeIntervention(inter2,client));
            System.out.println("Liste d'intervention de jean: "+ client.getListeIntervention());
            
            try {
                emp3 = (Employe) Service.authentifier("mec3","ahD3aing7");
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoResultException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Yvon occupé ?: " + emp3.estOccupe());
            System.out.println("Liste d'intervention de Yvon: "+ emp3.getMesInterventions());
            System.out.println("Intervention en cours de Yvon: "+ emp3.getInterventionEnCours().toString());
            
            Animal inter3 = new Animal("chat","Ma chat n'a pas de croquettes");
            System.out.println("Retour de la demande d'intervention: "+  Service.demandeIntervention(inter3,client));
            
            
            System.out.println("3 - Test Affichage opérations du jour");
            System.out.println("Aujourd'hui " +Service.recupererInterventionsDuJour());
            inter.setDebut(new Date("03/15/2019"));
            Service.validerIntervention(inter);
            System.out.println("Décalé  " +Service.recupererInterventionsDuJour());
            
            System.out.println("4 - Test interventions d'un client");
            System.out.println("Client, 1 intervention: " + Service.getInterventions(client));
            System.out.println("Client2, 0 intervention:   " + Service.getInterventions(client2));
            System.out.println();
            System.out.println();
            
            
            
            
            
            System.out.println("Fin du programme");
            
            JpaUtil.destroy();
            
            
            
            
            
      
        
        
 
    }
    
}
