/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.metier.modele;

import javax.persistence.Entity;

/**
 *
 * @author mmessalti
 */

@Entity
public class Livraison extends Intervention {

/**
     *
     * @param objet
     * @param entreprise
     */

    private String objet;
    private String entreprise;

    public Livraison() {
        super();
    }

    public Livraison(String objet, String entreprise) {
        super();
        this.objet = objet;
        this.entreprise = entreprise;
    }

    public String getObjet() {
        return objet;
    }
    public String getEntreprise() {
        return entreprise;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }
    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    @Override
    public String toString() {
        return "Livraison { "+super.toString() + ", objet=" + objet + ", entreprise=" + entreprise + '}';
    }
    

    
}
