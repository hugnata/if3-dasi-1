/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.dao;

import fr.insalyon.dasi.metier.modele.Personne;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author drevillore
 */
public class PersonneDao {
    
    /**Met à jour une personne en base de données
     *
     * @param p La personne à mettre à jour
     * @return
     */
    public static Personne Update(Personne p){
        EntityManager em = JpaUtil.obtenirEntityManager();
        return em.merge(p);
    }

    /**Crée une personne dans la base de donnée
     *
     * @param p 
     */
    public static void Create(Personne p){
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.persist(p);
    }
    
    /**Charge une personne par son identifiant
     *
     * @param id
     * @return
     */
    
    
    public static Personne Load(int id){
        EntityManager em = JpaUtil.obtenirEntityManager();
        return em.find(Personne.class,id);
    }

    /**Charge une personne par son nom d'utilisateur
     *
     * @param nomutilisateur
     * @return
     */
    public static Personne LoadByUsername(String nomutilisateur){
       EntityManager em = JpaUtil.obtenirEntityManager();
       String jpql = "select p from Personne p where p.userName = :user";
       Query query = em.createQuery(jpql);
       query.setParameter("user", nomutilisateur);
       return (Personne) query.getSingleResult();
    }
    
        /**Charge une personne par son nom d'utilisateur
     *
     * @param nomutilisateur
     * @return
     */
    public static boolean usernameExist(String nomutilisateur){
       EntityManager em = JpaUtil.obtenirEntityManager();
       String jpql = "select p from Personne p where p.userName = :user";
       Query query = em.createQuery(jpql);
       query.setParameter("user", nomutilisateur);
       return query.getResultList().size() != 0;
    }

    /**Supprime une personne en base de donnée
     *
     * @param p la personne à supprimer
     */
    public static void Delete(Personne p){
        EntityManager em = JpaUtil.obtenirEntityManager();
        em.remove(p);
    }
}
