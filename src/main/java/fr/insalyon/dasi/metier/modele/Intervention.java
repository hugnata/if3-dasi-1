/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.metier.modele;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.eclipse.persistence.jpa.jpql.parser.DateTime;

/**
 *
 * @author mmessalti
 */
@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
public class Intervention {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id; // correspond au n° d'intervention
    private Integer status; // 0 : inscit par client (non traité encore)
                            // 1 : attribué à un employe
                            // 2 : succes
                            // 3 : echec
    private String Description;
    /**La date de création de l'intervention
     */
    @Column(nullable=true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date debut; 
    /**La date de fin de l'intervention
     */
    @Column(nullable=true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;
    private String Commentaire;
    @ManyToOne
    private Client client;
    @ManyToOne
    private Employe employe;

    
    public Intervention() {
        this.status = 0;
        this.debut = new Date();
    }

    public Intervention(String Description) {
        this.Description = Description;
        this.status = 0;
        this.debut = new Date();
        
    }
    
    
    public Integer getId() {
        return id;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDescription() {
        return Description;
    }

    public Date getDebut() {
        return debut;
    }

    public Date getFin() {
        return fin;
    }

    public String getCommentaire() {
        return Commentaire;
    }

    public Client getClient() {
        return client;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public void setCommentaire(String Commentaire) {
        this.Commentaire = Commentaire;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    @Override
    public String toString() {
        return "(# " + id + " , status:" + status + ", client: " + client.getNomEntier() + ", employe: "+ employe.getNomEntier() + ", description " +Description;
    }
    
    /**Renvoie un string avec tout les infos de l'intervention, utile pour le débug
     * 
     * @return 
     */
    public String toStringComplet() {
        return "Intervention{" + "id=" + id + ", status=" + status + ", Description=" + Description + "\n debut=" + debut + ", fin=" + fin + ", Commentaire=" + Commentaire + ", client=" + client + ", employe=" + employe + '}';
    }
    
    
    
    
    
}
