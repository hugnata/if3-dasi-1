/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.dao;

import fr.insalyon.dasi.metier.modele.Employe;
import fr.insalyon.dasi.metier.modele.Personne;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author hreymond
 */
public class EmployeDao {
   
    public static List<Employe> getEmployeDispo(){  
       EntityManager em = JpaUtil.obtenirEntityManager();
       Date maintenant = new Date();
       String jpql = "select e from Employe e where e.interventionEnCours = NULL and :heure BETWEEN e.debut AND e.fin";
       Query query = em.createQuery(jpql);
       query.setParameter("heure", maintenant.getHours());
       return (List<Employe>) query.getResultList();
    }
    
}
