/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.metier.modele;

import com.google.maps.model.LatLng;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/** La classe personne représente une personne. Cette personne peut être un Client ou un Employé.
 * 
 *
 * @author mmessalti
 */
@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
public class Personne {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id; // c'est le numero de client lorsque la personne est un client
    @Column(nullable = false, unique = true)
    private String userName;
    @Column(nullable = false)
    private String passWord;
    private String telephone;
    private char civilite;
    private String nom;
    private String prenom;
    @Temporal(TemporalType.DATE)
    private Date dateNaissance;
    private String adresse;
    private String mail;
    private double latitude;
    private double longitude;
    
    /**Constructeur par défaut
     *
     */
    public Personne() {
    }
    
    /**Le constructeur paramétré de Personne
     *
     * @param userName
     * @param passWord
     */
    public Personne(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }

    /**Le constructeur paramétré de Personne
     *
     * @param userName
     * @param passWord
     * @param telephone
     * @param civilite
     * @param nom
     * @param prenom
     * @param dateNaissance
     * @param adresse
     * @param mail
     */
    public Personne(String userName, String passWord, String telephone, char civilite, String nom, String prenom, Date dateNaissance, String adresse, String mail) {
        this.userName = userName;
        this.passWord = passWord;
        this.telephone = telephone;
        this.civilite = civilite;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.adresse = adresse;
        this.mail = mail;
    }

    /**Renvoie l'identifiant de la Personne
     *
     * @return
     */
    public Integer getId() {
        return id;
    }


    /**
     *
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     *
     * @param passWord
     */
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    /**
     *
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @return
     */
    public String getPassWord() {
        return passWord;
    }

    /**
     *
     * @return
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     *
     * @param telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     *
     * @return
     */
    public char getCivilite() {
        return civilite;
    }

    /**
     *
     * @param civilite
     */
    public void setCivilite(char civilite) {
        this.civilite = civilite;
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     *
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     *
     * @return
     */
    public Date getDateNaissance() {
        return dateNaissance;
    }

    /**
     *
     * @param dateNaissance
     */
    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    /**
     *
     * @return
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     *
     * @param adresse
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     *
     * @return
     */
    public String getMail() {
        return mail;
    }

    /**
     *
     * @param mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double lat) {
        this.latitude = lat;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double lng) {
        this.longitude = lng;
    }
    
    public String getNomEntier() {
        return this.prenom + " " + this.nom.toUpperCase();
    }
    


    
    

    @Override
    public String toString() {
        return this.prenom +  " " + nom + " de type " + getClass().getSimpleName();
    }
    
    public String toStringComplet() {
        return "Personne{" + "id=" + id + ", telephone=" + telephone + ", civilite=" + civilite + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance + ", adresse=" + adresse + ", mail=" + mail + '}';
    }
}
