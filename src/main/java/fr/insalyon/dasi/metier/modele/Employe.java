/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.insalyon.dasi.metier.modele;

import fr.insalyon.dasi.util.Message;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author mmessalti
 */
@Entity
public class Employe extends Personne {
    private Integer debut; 
    private Integer fin;

    @OneToMany(mappedBy = "employe",cascade = CascadeType.ALL)
    private List<Intervention> mesInterventions;
    @OneToOne
    private Intervention interventionEnCours;

    public Employe() {
        super();
    }
    
    /**
     *
     * @param userName
     * @param passWord
     */
    public Employe(String userName, String passWord) {
        super(userName, passWord);

    }

    /**
     *
     * @param debut
     * @param fin
     * @param userName
     * @param passWord
     * @param telephone
     * @param civilite
     * @param nom
     * @param prenom
     * @param dateNaissance
     * @param adresse
     * @param mail
     */
    public Employe(Integer debut, Integer fin, String userName, String passWord, String telephone, char civilite, String nom, String prenom, Date dateNaissance, String adresse, String mail) {
        super(userName, passWord, telephone, civilite, nom, prenom, dateNaissance, adresse, mail);
        this.debut = debut;
        this.fin = fin;
    }

    /**
     *
     * @return
     */
    public Integer getDebut() {
        return debut;
    }

    /**
     *
     * @return
     */
    public Integer getFin() {
        return fin;
    }
    
    /**
     *
     * @return
     */
    public Boolean estOccupe() {
        // @TODO penser a mettre interventionEnCours a null quand est fini
        return this.interventionEnCours != null;
    }
    
    /**
     *
     * @param debut
     */
    public void setDebut(Integer debut) {
        this.debut = debut;
    }

    /**
     *
     * @param fin
     */
    public void setFin(Integer fin) {
        this.fin = fin;
    }

    public List<Intervention> getMesInterventions() {
         if(mesInterventions==null){
            return null;
        }
         mesInterventions.size();
        return mesInterventions;
    }

    public void setMesInterventions(List<Intervention> mesInterventions) {
        this.mesInterventions = mesInterventions;
    }

    public void attribuerIntervention(Intervention i){ 
        i.setEmploye(this);
        this.interventionEnCours = i;
        this.mesInterventions.size();
        this.mesInterventions.add(i);
  
    }

    public void validerIntervention() {
        this.interventionEnCours = null;
    }

    public Intervention getInterventionEnCours() {
        return interventionEnCours;
    }
}
